var Client = {}
Client.socket = io.connect()

var chatinput = document.getElementById("chatinput");
chatinput.addEventListener("keydown", function (e) {
    if (e.keyCode === 13) { // Check ENTER
        SendMessage();
    }
});

function SendMessage(){
    Client.socket.emit('chat', document.getElementById('chatinput').value)
    document.getElementById('chatinput').value = ""
}

Client.socket.on('chat', function(chatString){
    var date = new Date()
    var formattedDate = date.toLocaleString({ dateStyle: "short" })
    document.getElementById('messages').innerHTML = formattedDate + ": " + chatString + "</br>" + document.getElementById('messages').innerHTML;
})

Client.socket.on('updateInfo', function(infoString){
    document.getElementById('info').innerHTML = "Online: " + infoString
})

Client.socket.on('disconnect', () => {
    console.log("Disconnected")
})