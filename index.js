var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);

var onlineClients = 0

app.use("/", express.static("public"))

server.listen(process.env.PORT || 8081,function(){
    console.log('Listening on '+server.address().port);
})

io.on('connection',function(socket){

    onlineClients++
    io.emit("updateInfo", onlineClients)

    socket.on('chat',function(chatString){
        if(chatString != ""){
            io.emit("chat", chatString)
        }
    })

    socket.on('disconnect', function(socket){
        onlineClients--
    })
})