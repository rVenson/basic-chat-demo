# Basic Chat Demo

This chat was created using express and socket.io just for fun.

## Install

Dependencies:

* express
* socket.io

Install all using npm:

``sh
npm install
``

## Run

Start the server using:

``sh
node index.js
``

Open your web browser and access [http://localhost:8081](http://localhost:8081)
